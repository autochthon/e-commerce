import React from "react";

import { Route } from "react-router-dom";

import "./App.css";
import HomePage from "./pages/homepage/HomePage";

const HatsPage = () => (
  <div>
    <h1>HATS PAGE</h1>
  </div>
);

function App() {
  return (
    <div>
      {/* <HomePage /> */}
      <Route exact path="/hats" component={HomePage}/>
    </div>
  );
}
export default App;
