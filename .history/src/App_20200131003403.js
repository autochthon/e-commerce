import React from "react";

import { Switch, Route } from "react-router-dom";

import "./App.css";

import HomePage from "./pages/homepage/HomePage";
import Shop from "./pages/shop/Shop";
import Header from "./components/header/Header";
import SignInAndSignUp from "./pages/sign-in-and-sign-up/SignInAndSignUp"

function App() {
  return (
    <div>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/shop" component={Shop} />
        <Route exact path="/sign-in" component={SignInAndSignUp} />
      </Switch>
    </div>
  );
}
export default App;
