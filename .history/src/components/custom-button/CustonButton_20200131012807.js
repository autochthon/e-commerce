import React from "react";

import "./custon-button.scss";

const CustonButton = ({ children, ...otherButtonProps }) => {
  return (
    <div className="custom-button" {...otherButtonProps}>
      {children}
    </div>
  );
};

export default CustonButon;
