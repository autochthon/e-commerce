import React from "react";

import "./custon-button.scss";

const CustonButton = ({ children, ...otherButtonProps }) => {
  return (
    <button className="custom-button" {...otherButtonProps}>
      {children}
    </button>
  );
};

export default CustonButton;
