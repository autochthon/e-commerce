import React from "react";

import "./custon-button.scss";

const CustonButton = ({ children, isGoogleSignIn, ...otherButtonProps }) => {
  return (
    <button
      className={`${isGoogleSignIn ? "google-sign-in" : ""} custon-button`}
      {...otherButtonProps}
    >
      {children}
    </button>
  );
};

export default CustonButton;
