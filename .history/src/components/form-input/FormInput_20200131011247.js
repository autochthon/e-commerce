import React from "react";

import "./form-input.scss"

const FormInput = ({ onChangeHandler, label, ...otherFormProps }) => {
  return (
    <div className="group">
      <input
        className="form-input"
        onChange={onChangeHandler}
        {...otherFormProps}
      />
      {label ? (
        <label
          htmlFor=""
          className={`${
            otherFormProps.value.length ? "shrink" : ""
          }form-input-label`}
        >
          {label}
        </label>
      ) : null}
    </div>
  );
};

export default FormInput;
