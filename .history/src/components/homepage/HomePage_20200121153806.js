import React from "react";

const HomePage = () => (
  <div className="homepage">
    <div className="directory-menu">
      <div className="menu__item">
        <div className="content">
          <h1 className="content__title">HATS</h1>
          <span className="content__subtitle">SHOP NOW</span>
        </div>
      </div>
      <div className="menu__item">
        <div className="content">
          <h1 className="content__title">JACKETS</h1>
          <span className="content__subtitle">SHOP NOW</span>
        </div>
      </div> <div className="menu__item">
        <div className="content">
          <h1 className="content__title">SNEAKERS</h1>
          <span className="content__subtitle">SHOP NOW</span>
        </div>
      </div> <div className="menu__item">
        <div className="content">
          <h1 className="content__title">WOMENS</h1>
          <span className="content__subtitle">SHOP NOW</span>
        </div>
      </div> <div className="menu__item">
        <div className="content">
          <h1 className="content__title">MENS</h1>
          <span className="content__subtitle">SHOP NOW</span>
        </div>
      </div>
    </div>
  </div>
);


export default HomePage;
