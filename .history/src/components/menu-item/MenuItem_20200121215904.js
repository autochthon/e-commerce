import React from "react";

import "./menu-item.scss";

const MenuItem = ({ title, imageUrl }) => {
  return (
    <div
      style={{ backgroundImage: `url(${imageUrl})` }}
      className="dir-menu__item"
    >
      <div className="content">
        <div className="content__title">{title}</div>
        <div className="content__subtitle">SHOP NOW</div>
      </div>
    </div>
  );
};

export default MenuItem;
