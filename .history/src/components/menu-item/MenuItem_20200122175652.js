import React from "react";
import {withRouter} from 'react-router-dom';
import "./menu-item.scss";

const MenuItem = ({ title, imageUrl, size ,history}) => {
  return (
    <div className={`${size} dir-menu__item`}>
      <div
        className="background-image"
        style={{ backgroundImage: `url(${imageUrl})` }}
      />
      <div className="content">
        <div className="content__title">{title.toUpperCase()}</div>
        <div className="content__subtitle">SHOP NOW</div>
      </div>
    </div>
  );
};

export default withRouter(MenuItem);
