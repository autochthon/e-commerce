import React, { Component } from "react";

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }
  render() {
    return (
      <div className="sign-in">
        <h2>I have an account</h2>
        <span>Sign in with your email</span>

        <form action="">
          <input
            type="email"
            name="email"
            value={this.state.email}
            id=""
            required
          />
          <input
            type="password"
            name="password"
            value={(this, this.state.password)}
            id=""
            required
          />

          <input type="submit" value="Submit Form" />
        </form>
      </div>
    );
  }
}
export default SignIn;
