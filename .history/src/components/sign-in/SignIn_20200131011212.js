import React, { Component } from "react";
import FormInput from "../form-input/FormInput";

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }

  onSumitHandler = e => {
    e.preventDefault();
    this.setState({ email: "", password: "" });
  };
  onChangeHandler = e => {
    const { value, name } = e.target;

    this.setState({ [name]: value });
  };

  render() {
    return (
      <div className="sign-in">
        <h2>I have an account</h2>
        <span>Sign in with your email</span>

        <form onSumit={this.onSumitHandler}>
          <FormInput
            type="email"
            name="email"
            value={this.state.email}
            id=""
            onChangeHandler={this.onChangeHandler}
            required
          />
          <label htmlFor="email">Email</label>
          <FormInput
            type="password"
            name="password"
            value={(this, this.state.password)}
            id=""
            onChangeHandler={this.onChangeHandler}
            required
          />
          <label htmlFor="password">Password</label>

          <input type="submit" value="Submit Form" />
        </form>
      </div>
    );
  }
}
export default SignIn;
