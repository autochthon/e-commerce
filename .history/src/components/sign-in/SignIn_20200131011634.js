import React, { Component } from "react";
import "./sign-in.scss";

import FormInput from "../form-input/FormInput";

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }

  onSumitHandler = e => {
    e.preventDefault();
    this.setState({ email: "", password: "" });
  };
  onChangeHandler = e => {
    const { value, name } = e.target;

    this.setState({ [name]: value });
  };

  render() {
    return (
      <div className="sign-in">
        <h2>I have an account</h2>
        <span>Sign in with your email</span>

        <form onSumit={this.onSumitHandler}>
          <FormInput
            type="email"
            name="email"
            value={this.state.email}
            id=""
            label="email"
            onChangeHandler={this.onChangeHandler}
            required
          />

          <FormInput
            type="password"
            name="password"
            value={(this, this.state.password)}
            id=""
            label="password"
            onChangeHandler={this.onChangeHandler}
            required
          />

          <input type="submit" value="Submit Form" />
        </form>
      </div>
    );
  }
}
export default SignIn;
