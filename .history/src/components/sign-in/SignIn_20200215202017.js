import React, { Component } from "react";
import "./sign-in.scss";

import { signInWithGoogle } from "../../firebase/firebase";
import FormInput from "../form-input/FormInput";
import CustonButton from "../custom-button/CustonButton";

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }

  onSumitHandler = e => {
    e.preventDefault();
    this.setState({ email: "", password: "" });
  };
  onChangeHandler = e => {
    const { value, name } = e.target;

    this.setState({ [name]: value });
  };

  render() {
    return (
      <div className="sign-in">
        <h2 className="title">I have an account</h2>
        <span>Sign in with your email</span>

        <form onSubmit={this.onSumitHandler}>
          <FormInput
            type="email"
            name="email"
            value={this.state.email}
            label="email"
            onChangeHandler={this.onChangeHandler}
            required
          />

          <FormInput
            type="password"
            name="password"
            value={(this, this.state.password)}
            label="password"
            onChangeHandler={this.onChangeHandler}
            required
          />

          <div className="buttons">
            <CustonButton type="submit">SIGN IN</CustonButton>
            <CustonButton onClick={signInWithGoogle} isGoogleSignIn>
              Sign in with Google
            </CustonButton>
          </div>
        </form>
      </div>
    );
  }
}
export default SignIn;
