import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyANorF4ToFHj70lFHQaoPnSRMn3f7GJ4ys",
  authDomain: "piti-s-shop.firebaseapp.com",
  databaseURL: "https://piti-s-shop.firebaseio.com",
  projectId: "piti-s-shop",
  storageBucket: "piti-s-shop.appspot.com",
  messagingSenderId: "495555402259",
  appId: "1:495555402259:web:2b0c28fc3b86bd02203de6",
  measurementId: "G-2MB2ZPBJJB"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// firebase.analytics();

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
