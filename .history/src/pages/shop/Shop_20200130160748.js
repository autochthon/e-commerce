import React, { Component } from "react";

import SHOP_DATA from "./ShopData";
import CollectionPreview from "../../components/preview/CollectionPreview";

class Shop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collections: SHOP_DATA
    };
  }

  render() {
    return (
      <div className="shop-preview">
        <CollectionPreview items={this.state.collections} />
      </div>
    );
  }
}
export default Shop;
