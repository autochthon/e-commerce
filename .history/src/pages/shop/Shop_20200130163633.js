import React, { Component } from "react";

import SHOP_DATA from "./ShopData";
import CollectionPreview from "../../components/preview/CollectionPreview";

class Shop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collections: SHOP_DATA
    };
  }

  render() {
    const { collections } = this.state;

    return (
      <div className="shop-preview">
        {collections
          //.filter((item, idx) => idx < 4)
          .map(({ id, ...otherProps }) => (
            <CollectionPreview key={id} {...otherProps} />
          ))}
      </div>
    );
  }
}
export default Shop;
