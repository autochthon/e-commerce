import React from "react";

import "./sign-in-and-sign-up.scss";

import SignIn from "../../components/sign-in/SignIn"

const SignInAndSignUp = () => {
  return (
    <div>
      <SignIn />
    </div>
  );
};

export default SignInAndSignUp;
